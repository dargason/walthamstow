#!/bin/sh

ENGRAVER_DIR=../../engraver
SESSIONS_DIR=../../tunes/sessions

WORK_DIR=$(mktemp -d)
echo "$WORK_DIR"

# Start with the specially chosen easy tunes

python "$ENGRAVER_DIR/formatter.py" -o "$WORK_DIR/tunes.ly" -i "$SESSIONS_DIR" -p "$ENGRAVER_DIR/preamble/a4x4/full.ly.fragment" -S -X - < ids/easy.ids
lilypond -o $WORK_DIR/out $WORK_DIR/tunes.ly
pdfnup --nup 1x4 --no-landscape --suffix easy $WORK_DIR/out.pdf


#python ../tunes/indexer.py ids -s ../tunes/index.csv -p 6 -f WFS -k name > ids/main.ids
python "$ENGRAVER_DIR/formatter.py" -o "$WORK_DIR/tunes.ly" -i "$SESSIONS_DIR" -p "$ENGRAVER_DIR/preamble/a4x4/full.ly.fragment" -S -X - < ids/main.ids
lilypond -o $WORK_DIR/out $WORK_DIR/tunes.ly
pdfnup --nup 1x4 --no-landscape --suffix main $WORK_DIR/out.pdf

# Make crib sheets for tunes with medium popularity (include above for ease of use)

#python ../tunes/indexer.py ids -s ../tunes/index.csv -p 2 -f WFS -k name > ids/crib.ids
python "$ENGRAVER_DIR/formatter.py" -o "$WORK_DIR/tunes.ly" -i "$SESSIONS_DIR" -p "$ENGRAVER_DIR/preamble/a4x4/crib.ly.fragment" -c -X - < ids/crib.ids
lilypond -o $WORK_DIR/out $WORK_DIR/tunes.ly
pdfnup --nup 1x4 --no-landscape --suffix crib $WORK_DIR/out.pdf

# Stitch up with front page of original tune book

pdfjam walthamstow-intro.pdf 1-3 out-easy.pdf - out-main.pdf - out-crib.pdf - \
       walthamstow-intro.pdf 4- -o out.pdf

#rm out-easy.pdf out-main.pdf out-crib.pdf


#lilypond -f png -dresolution=300 -o out/out out/out.ly
#eog out/*.png
